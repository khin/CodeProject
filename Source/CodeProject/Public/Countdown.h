// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Countdown.generated.h"

UCLASS()
class CODEPROJECT_API ACountdown : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACountdown();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;


	FTimerHandle countdownTimerHandle;

	//How long, in seconds, this timer runs
	UPROPERTY(EditAnywhere)
	int32 countdownTime;

	UTextRenderComponent *countdownText;

	void UpdateTimerDisplay();

	void AdvanceTimer();

	UFUNCTION(BlueprintNativeEvent)
	void OnCountdownFinished();
	virtual void OnCountdownFinished_Implementation();
	
};
