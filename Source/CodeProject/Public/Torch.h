// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Torch.generated.h"

UCLASS()
class CODEPROJECT_API ATorch : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATorch();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Maximum level of fuel allowed
	UPROPERTY(EditAnywhere)
		float maxFuel;

	// How much fuel is spent per second
	UPROPERTY(EditAnywhere)
		float fuelRate;

	// Current amount of fuel remaining
	UPROPERTY(EditAnywhere)
		float currentFuel;

	// Is the torch burning?
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		bool isBurning;

	// Is the torch doused with water?
	UPROPERTY(EditAnywhere)
		bool doused;

	UFUNCTION(BlueprintCallable, Category="Torch")
		bool CanBurn();

	UFUNCTION(BlueprintNativeEvent)
		void Ignite();
	virtual void Ignite_Implementation();

	UFUNCTION(BlueprintNativeEvent)
		void AddFuel(float amount);
	virtual void AddFuel_Implementation(float amount);

	UFUNCTION(BlueprintNativeEvent)
		void Douse();
	virtual void Douse_Implementation();

	UFUNCTION(BlueprintNativeEvent)
		void TurnOff();
	virtual void TurnOff_Implementation();

	UFUNCTION(BlueprintNativeEvent)
		void Activated();
	virtual void Activated_Implementation();

private:
	UFUNCTION()
		void Burn(float deltaTime);
};
