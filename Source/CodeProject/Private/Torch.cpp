// Fill out your copyright notice in the Description page of Project Settings.

#include "CodeProject.h"
#include "Torch.h"


// Sets default values
ATorch::ATorch()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	maxFuel = 20.0f;
	fuelRate = 2.0f;
	currentFuel = maxFuel;

	isBurning = false;
	doused = false;
}

// Called when the game starts or when spawned
void ATorch::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ATorch::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (isBurning)
		Burn(DeltaTime);
}

void ATorch::Burn(float dt) {
	currentFuel -= fuelRate * dt;

	if (currentFuel <= 0) {
		currentFuel = 0;
		TurnOff();
	}
}

void ATorch::Ignite_Implementation() {
	if (!CanBurn())
		return;

	isBurning = true;
}

void ATorch::AddFuel_Implementation(float amount) {
	currentFuel += amount;

	if (currentFuel > maxFuel)
		currentFuel = maxFuel;
}

void ATorch::Douse_Implementation(){
	doused = true;
	currentFuel = 0.0f;

	if (isBurning)
		TurnOff();
}

void ATorch::TurnOff_Implementation(){
	isBurning = false;
}

void ATorch::Activated_Implementation(){
	if (!isBurning)
		Ignite();
	else
		Douse();
}

bool ATorch::CanBurn() {
	return currentFuel > 0.0f && !doused;
}
